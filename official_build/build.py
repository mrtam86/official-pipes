import os
import sys
import glob
import json
import traceback

from email import utils
from datetime import datetime
from typing import List, Optional
from dataclasses import dataclass, asdict
from concurrent.futures.thread import ThreadPoolExecutor

import yaml
import cerberus
from requests.exceptions import HTTPError

from official_build.logger import logger
from official_build.constants import (
    CREATED_AT_PIPES, REMOTE_METADATA_SCHEMA,
    LOCAL_METADATA_SCHEMA, FINAL_METADATA_SCHEMA
)
from official_build.api.bitbucket.base import BitbucketApiService, BitbucketApiServiceError
from official_build.api.git.base import GitApiService
from official_build.validators import (
    Failures, RuleContext, PipeValidatorHandler, PipeManifestFileValidator,
    PipeLatestVersionValidator, PipeGitTagValidator, PipeReadmeValidator,
    PipeYmlFragmentValidator, PipeDockerImageSizeValidator,
    PipeRemoteYamlValidator, PipeFinalYamlValidator
)


@dataclass
class ProcessMetadata:
    file: str
    valid: bool
    failures: Failures
    errors: List[str]


@dataclass
class ProcessResult:
    metadata: ProcessMetadata
    data: Optional[dict]


def validate(context: RuleContext) -> tuple:
    # RuleEngine
    pmm = PipeValidatorHandler(
        context,
        [
            PipeManifestFileValidator,
            PipeLatestVersionValidator,
            PipeGitTagValidator,
            PipeReadmeValidator,
            PipeYmlFragmentValidator,
            PipeDockerImageSizeValidator,
            PipeRemoteYamlValidator,
            PipeFinalYamlValidator,
        ]
    )

    return pmm.validate()


def extract_information(local_yml: dict, infile: str) -> RuleContext:
    bitbucket_api_service = BitbucketApiService()

    # validate local metadata
    LOCAL_METADATA = cerberus.Validator(LOCAL_METADATA_SCHEMA)
    local_yml_valid = LOCAL_METADATA.validate(local_yml)
    if not local_yml_valid:
        logger.warning(
            f"Structure you are using in the official-pipes metadata is deprecated. "
            f"Please, update to the current one: {infile}"
        )

    final_yml = local_yml.copy()

    remote_yml = None

    repository_path = final_yml['repositoryPath']
    version = final_yml['version']

    # Check is repository exists
    try:
        repo_info = bitbucket_api_service.get_repository_info(repository_path)
    except HTTPError as error:
        logger.exception(error)
        raise BitbucketApiServiceError()

    # Check is repository's tag (pipe's version) exists
    try:
        bitbucket_api_service.is_version_exists(repository_path, version)
    except HTTPError as error:
        logger.exception(error)
        raise BitbucketApiServiceError()

    # Fetch logo from the repository info
    if 'logo' not in final_yml:
        final_yml['logo'] = repo_info['links']['avatar']['href']
    # Fetch yaml definition from the README.md
    if 'yml' not in final_yml:
        try:
            yml_definition = bitbucket_api_service.get_yml_definition(repository_path, version)
        except HTTPError as error:
            yml_definition = None
            logger.debug(error)

        final_yml['yml'] = '' if yml_definition is None else yml_definition

    # Fetch the remote pipe.yml (metadata)
    try:
        remote_yml = bitbucket_api_service.get_pipe_metadata(repository_path, version)
    except HTTPError as error:
        logger.debug(error)

    if remote_yml is None:
        remote_yml = {}

    REMOTE_METADATA = cerberus.Validator(REMOTE_METADATA_SCHEMA)
    remote_yml_valid = REMOTE_METADATA.validate(remote_yml)
    if not remote_yml_valid:
        logger.warning(
            f"Structure you are using in the metadata (pipe.yml) is deprecated. "
            f"Please, update to the current one: {repository_path}"
        )

    # TODO: remove once custom pipes will change to new version
    if 'maintainer' in final_yml:
        remote_yml.pop('maintainer', None)
    if 'vendor' in final_yml:
        remote_yml.pop('vendor', None)

    final_yml = {key: value for key, value in {**final_yml, **remote_yml}.items() if key in FINAL_METADATA_SCHEMA}

    # Fetch pipe's published or added date
    final_yml['created_at'] = extract_git_pipe_created_at(infile)

    # deprecated move all logic to created_at
    # timestamp format rfc2822
    timestamp = utils.format_datetime(datetime.fromisoformat(final_yml['created_at']))
    final_yml['timestamp'] = f"\"b'{timestamp}\\n'\""

    # TODO add final_yml['updated_at'] as datetime of the current version

    return RuleContext(infile=infile, local_yml=local_yml, remote_yml=remote_yml, final_yml=final_yml)


def extract_git_pipe_created_at(infile: str) -> str:
    if infile in CREATED_AT_PIPES:
        return CREATED_AT_PIPES.get(infile)

    git_api_service = GitApiService()
    released_date = git_api_service.get_pipe_published_at(infile, 'iso-strict')
    file_added_date = git_api_service.get_pipe_file_added_at(infile, 'iso-strict')

    return released_date if released_date else file_added_date


def process(infile: str) -> ProcessResult:
    failures = Failures(criticals=[], warnings=[], skipped=[])
    errors = []
    context = None
    yaml_file = None

    with open(infile, 'r') as stream:
        try:
            manifest = yaml.safe_load(stream)
        except Exception as e:
            errors.append(repr(e))

    try:
        context = extract_information(local_yml=manifest, infile=infile)
        yaml_file = context.final_yml
    except Exception as e:
        errors.append(f"Error extract_information: "
                      f"{''.join(traceback.TracebackException.from_exception(e).format())}")
    if context:
        try:
            failures = validate(context)
        except Exception as e:
            errors.append(f"Error validation: "
                          f"{''.join(traceback.TracebackException.from_exception(e).format())}")

    print(
        f"Pipe: {infile}. "
        f"Criticals: {len(failures.criticals)} "
        f"Warnings: {len(failures.warnings)} "
        f"Errors: {len(errors)} "
        f"Skipped: {len(failures.skipped)}"
    )
    return ProcessResult(
        metadata=ProcessMetadata(
            file=infile,
            valid=(
                len(failures.criticals) == 0 and
                len(failures.warnings) == 0 and
                len(errors) == 0
            ),
            failures=failures,
            errors=errors),
        data=yaml_file)


def main():
    work_dir = os.getcwd()
    path = sys.argv[1] if len(sys.argv) > 1 else 'pipes/*.yml'
    list_files = sorted(glob.glob(path))

    logger.info("Starting: " + str(len(list_files)) + " pipe(s) to verify.")

    t1 = datetime.now()

    with ThreadPoolExecutor() as executor:
        results = list(executor.map(process, list_files))

    errors = [x for x in results if not x.metadata.valid]
    build_is_failed = False

    if errors:
        for error in errors:
            if error.data is None:
                logger.error(f"{asdict(error.metadata)}")
                build_is_failed = True
                continue

            maintainer = error.data.get('maintainer', None)
            # TODO: remove when all pipes migrate to new metadata
            maintainer_name = maintainer['name'] if isinstance(maintainer, dict) else maintainer
            if maintainer_name == 'Atlassian':
                logger.error(f"{asdict(error.metadata)}")
                # fail build for any Atlassian's pipes error
                build_is_failed = True
            elif error.metadata.failures.criticals:
                logger.error(f"{asdict(error.metadata)}")
                # fail build for any critical failures
                build_is_failed = True
            else:
                logger.warning(f"{asdict(error.metadata)}")

    if build_is_failed:
        logger.error("Found critical errors. Build has failed.")
        sys.exit(1)

    data = [result.data for result in results if result.data]

    with open(os.path.join(work_dir, 'pipes.json'), 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)

    t2 = datetime.now()
    logger.info("Finished. Time: " + str(t2 - t1))
